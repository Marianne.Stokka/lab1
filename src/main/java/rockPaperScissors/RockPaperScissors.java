package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    List<String> ynChoices = Arrays.asList("y", "n");


    
    // Find randome element from rpsChoices for computer
    public String randomeChoice() {
        Random random = new Random();
        String randomeElement = rpsChoices.get(random.nextInt(rpsChoices.size()));
        return randomeElement;
    }

    // return human choice if it is valid, else ask to try again
    public String userChoice() {
        while (true) {
            String rpsUserChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (rpsChoices.contains(rpsUserChoice)) {
                return rpsUserChoice;
            }
            else {
                System.out.println("I do not understand " + rpsUserChoice + ". Could you try again?");
            }
        }
    }

    public static boolean humanWin(String user1, String user2) {
        if (user1.equals("rock")) {
            return user2.equals("scissors"); 
        }
        else if (user1.equals("scissors")) {
            return user2.equals("paper");
        }
        else {
            return user2.equals("rock");
        }
    }

    public static boolean computerWin(String user1, String user2) {
        if (user2.equals("rock")) {
            return user1.equals("scissors"); 
        }
        else if (user2.equals("scissors")) {
            return user1.equals("paper");
        }
        else {
            return user1.equals("rock");
        }
    }

    public String continueToPlay() {
        while (true) {
            String continueAnswar = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (ynChoices.contains(continueAnswar)) {
                return continueAnswar;
            }
            else {
                System.out.println("I do not understand " + continueAnswar + ". Could you try again?");
            }
        }

    }
    
    public void run() {

        // Human and computer choice
        while (true) {
            System.out.println("Let's play round " + roundCounter);
            String humanChoice = userChoice();
            String computerChoice = randomeChoice();
            String choiceString = "Human chose " + humanChoice + ", computer chose " + computerChoice + ".";
            
            // Check who wins
            if (humanWin(humanChoice, computerChoice)) {
                System.out.println(choiceString + " Human wins!");
                humanScore++;
            }
            else if (computerWin(humanChoice, computerChoice)){
                System.out.println(choiceString + " Computer wins!");
                computerScore++;
            }
            else {
                System.out.println(choiceString + " It's a tie!");
            }

            // Score
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            // Continue playing?
            String continueAnswar = continueToPlay();
            if (continueAnswar.equals("n")) {
                break;
            }
            else {
                roundCounter++;
            }
        }

        System.out.println("Bye bye :)");
        
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
